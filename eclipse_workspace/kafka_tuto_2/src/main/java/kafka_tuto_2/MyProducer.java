package kafka_tuto_2;

import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

public class MyProducer {

  public static void main(String[] args) {
    new MyProducer();
  }

  public MyProducer() {
    KafkaProducer<Void, String> kafkaProducer = new KafkaProducer<>(configureKafkaProducer());

    try {
      int i = 0;
      String message;

      // Infinite loop for producing messages
      while (true) {
        message = "{ \"number\": " + i++ + "}";
        kafkaProducer.send(new ProducerRecord<Void, String>("test", null, message));

        // Wait for 1 second before sending the next message
        TimeUnit.SECONDS.sleep(1);
      }
    } catch (Exception e) {
      System.err.println("Something went wrong: " + e.getMessage());
    } finally {
      kafkaProducer.close();
    }
  }

  private Properties configureKafkaProducer() {
    Properties producerProperties = new Properties();
    producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.VoidSerializer");
    producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.StringSerializer");
    return producerProperties;
  }
}
